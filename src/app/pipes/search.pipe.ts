import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform (items: any, searchTerms: any): any {
    if (searchTerms && Array.isArray(items)) {
      let filterKeys = Object.keys(searchTerms);
      return items.filter(item =>
        filterKeys.reduce((memo, keyName) =>
          (memo && new RegExp(searchTerms[keyName], 'gi').test(item[keyName])) || searchTerms[keyName] === "", true));
    } 
    else {
      return items;
    }
  }
}
