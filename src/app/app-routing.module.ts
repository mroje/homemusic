import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { BandListComponent } from './components/band-list/band-list.component';
import { BandTracksComponent } from './components/band-tracks/band-tracks.component';
import { FavoriteTracksComponent } from './components/favorite-tracks/favorite-tracks.component';
import { QueueComponent } from './components/queue/queue.component';
import { SearchResultComponent } from './components/search-result/search-result.component';

import { QueueToggleComponent } from './components/queue-toggle/queue-toggle.component';

const routes: Route[] = [
  { path: '', redirectTo: '/bands', pathMatch: 'full' },
  { path: 'bands', component: BandListComponent },
  { path: 'bands/:bandId', component: BandTracksComponent },
  { path: 'favorite-tracks', component: FavoriteTracksComponent },
  { path: 'queue', component: QueueComponent },
  { path: 'search-results/:searchTerm', component: SearchResultComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
