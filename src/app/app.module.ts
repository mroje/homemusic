import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BandListComponent } from './components/band-list/band-list.component';
import { BandTracksComponent } from './components/band-tracks/band-tracks.component';
import { FavoriteTracksComponent } from './components/favorite-tracks/favorite-tracks.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { QueueComponent } from './components/queue/queue.component';
import { QueueToggleComponent } from './components/queue-toggle/queue-toggle.component';
import { SearchResultComponent } from './components/search-result/search-result.component';

import { SearchPipe } from './pipes/search.pipe';
import { FilterPipe } from './pipes/filter.pipe';

import { ApiService } from './services/api.service';
import { AppService } from './services/app.service';
import { BandService } from './services/band.service';
import { TrackService } from './services/track.service';
import { SearchService } from './services/search.service';

@NgModule({
  declarations: [
    AppComponent,
    BandListComponent,
    BandTracksComponent,
    FavoriteTracksComponent,
    MainMenuComponent,
    QueueComponent,
    QueueToggleComponent,
    SearchResultComponent,
    SearchPipe,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    ApiService,
    AppService,
    BandService,
    TrackService,
    SearchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
