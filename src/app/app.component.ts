import { Component, OnInit, ViewChild, HostListener } from '@angular/core';

import { environment } from '../environments/environment';

import { AppService } from './services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  @ViewChild('mainMenu') mainMenuElement;
  @ViewChild('audioPlayer') audioPlayerElement;
  audioPlayer: any;

  constructor (public appService: AppService) {}

  ngOnInit () {
    this.audioPlayer = this.audioPlayerElement.nativeElement;

    this.appService.onTrackClick.subscribe((track: any) => {
      this.audioPlayer.src = `${environment.url}api/tracks/${track.id}/stream`;
      this.audioPlayer.play();
    });

    this.audioPlayer.addEventListener('ended', () => {
      this.appService.onTrackEnd.emit();      
    });
  }

  @HostListener('click', ['$event.target'])
  onClick (clickedElement) {
    const isClickedOutsideOfOpenedMainMenu = !this.mainMenuElement.nativeElement.contains(clickedElement) 
      && this.appService.isMainMenuOpened;

    if (isClickedOutsideOfOpenedMainMenu) {
      this.appService.collapseMainMenu();
    }
  }
}
