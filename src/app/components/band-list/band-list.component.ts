import { Component, OnInit } from '@angular/core';

import { AppService } from '../../services/app.service';
import { BandService } from './../../services/band.service';

@Component({
  templateUrl: './band-list.component.html'
})
export class BandListComponent implements OnInit {
    
  bands: any[];

  constructor (private appService: AppService, private bandService: BandService) {}
  
  ngOnInit() {
    this.appService.onSyncComplete.subscribe(() => {
      this.getBands();
    });

    this.getBands();
  }

  private getBands () {
    this.bands = [];

    this.bandService.getAll()
      .then(result => {
        this.bands = result;
      });
  }
}