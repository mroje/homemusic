import { Component, OnInit } from '@angular/core';

import { AppService } from './../../services/app.service';
import { TrackService } from '../../services/track.service';

@Component({
  templateUrl: './queue.component.html'
})
export class QueueComponent implements OnInit {
    
  tracks: any[];

  constructor (
    private appService: AppService,
    private trackService: TrackService
  ) {}

  ngOnInit() {
    this.trackService.getQueued()
      .then(result => {
        this.tracks = result;
        
        this.tracks.forEach((t: any) => {
          t.isQueued = true;
          let { playingTrack } = this.appService;
          if (playingTrack) {
            t.isPlaying = t.id === playingTrack.id;
          }
        });
      });

    this.appService.onTrackEnd.subscribe(() => {
      let nextTrack = this.getNextTrack();
      if (nextTrack) {
        this.playTrack(nextTrack);
      }
    });
  }

  onTrackClicked (track: any) {
    this.playTrack(track);
  }

  getNextTrack() {
    let currentTrack = this.tracks.find((t: any) => { return t.isPlaying === true; });
    let currentTrackIndex = this.tracks.indexOf(currentTrack);
    return this.tracks[currentTrackIndex + 1];
  }

  playTrack (track: any) {
    this.tracks.map((t: any) => t.isPlaying = false);
    track.isPlaying = true;
    
    this.appService.onTrackClick.emit(track);
  }
}
