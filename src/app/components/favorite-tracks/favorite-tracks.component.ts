import * as _ from 'lodash';

import { Component, OnInit } from '@angular/core';

import { AppService } from '../../services/app.service';
import { TrackService } from '../../services/track.service';

@Component({
  templateUrl: './favorite-tracks.component.html'
})
export class FavoriteTracksComponent implements OnInit {
    
  favoriteTracks: any[];
  searchTerm: string;

  constructor (
    private appService: AppService,
    private trackService: TrackService
  ) {}

  ngOnInit() {
    this.trackService.getFavorites()
      .then(result => {                
        this.favoriteTracks = result;
      });
  }

  onTrackClicked (track: any) {
    this.favoriteTracks.map((t: any) => t.isPlaying = false);
    track.isPlaying = true;

    this.appService.onTrackClick.emit(track);
  }

  toggleFavorite (track: any) {
    this.trackService.toggleFavorite(track.id)
      .then(result => {
        _.remove(this.favoriteTracks, track);
      });
  }
}
