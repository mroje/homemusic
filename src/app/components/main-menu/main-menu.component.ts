import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { AppService } from '../../services/app.service';

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html'
})
export class MainMenuComponent implements OnInit, OnDestroy {

  mutationObserver: any;
  syncInProgress: boolean = false;

  constructor (private router: Router, private appService: AppService) {}

  ngOnInit () {
    const mutationObserverTarget = document.querySelector('.main-menu-collapsible-content');

    const mutationObserverCallback = (mutationsList, observer) => {
      for (let mutation of mutationsList) {
        const { classList } = mutation.target;

        if (classList.contains('collapsing') || classList.contains('in')) {
          this.appService.isMainMenuOpened = true;
        }
        else {
          this.appService.isMainMenuOpened = false;
        }
      }
    };

    this.mutationObserver = new MutationObserver(mutationObserverCallback);
    this.mutationObserver.observe(mutationObserverTarget, { attributes: true, attributeFilter: ['class'] });
  }

  ngOnDestroy () {
    this.mutationObserver.disconnect();
  }

  onRouteLinkClick (): void {
    this.appService.collapseMainMenu();
  }

  onSearchClick (searchTerm: string) {
    this.router.navigate(['search-results', searchTerm]);
  }

  async onSyncClick () {
    if (this.syncInProgress) { return; }
    this.syncInProgress = true;

    try {
      await this.appService.updateDatabase();
      this.syncInProgress = false;

      this.appService.onSyncComplete.emit();

      alert("Sync succeeded!");
    }
    catch (error) {
      // TODO: Handle error 
    }
  }

  changeColorTheme () {
    this.appService.changeColorTheme();
  }
}
