import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AppService } from '../../services/app.service';
import { BandService } from '../../services/band.service';
import { TrackService } from '../../services/track.service';

@Component({
  templateUrl: './band-tracks.component.html'
})
export class BandTracksComponent implements OnInit {
    
  band: any;
  searchTerm: string;

  constructor (
    private appService: AppService,
    private bandService: BandService,
    private trackService: TrackService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {

      this.bandService.getOneById(params.bandId)
        .then(result => {
          this.band = result;
        });
    });
  }

  onTrackClicked (track: any) {
    this.band.tracks.map((t: any) => t.isPlaying = false);
    track.isPlaying = true;
    
    this.appService.onTrackClick.emit(track);
  }

  toggleFavorite (track: any) {
    this.trackService.toggleFavorite(track.id)
      .then(result => {
        track.isFavorite = !track.isFavorite;
      });
  }

  goBack() {
    this.location.back();
  }
}
