import { Component, Input, OnChanges } from '@angular/core';

import { TrackService } from '../../services/track.service';

@Component({
  selector: 'queue-toggle',
  templateUrl: './queue-toggle.component.html'
})
export class QueueToggleComponent implements OnChanges {

  @Input() track: any;

  constructor (private trackService: TrackService) {}

  ngOnChanges (changes: any) {
    this.trackService.isQueued(changes.track.currentValue)
      .then(result => {
        this.track.isQueued = result;
      });
  }

  enqueueTrack() {
    this.trackService.enqueue(this.track);
    this.track.isQueued = true;
  }

  dequeueTrack() {
    this.trackService.dequeue(this.track);
    this.track.isQueued = false;
  }
}
