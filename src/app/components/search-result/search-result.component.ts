import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AppService } from '../../services/app.service';
import { SearchService } from '../../services/search.service';
import { TrackService } from '../../services/track.service';

@Component({
  templateUrl: './search-result.component.html'
})
export class SearchResultComponent implements OnInit {
    
  tracks: [any];

  constructor (
    private appService: AppService,
    private searchService: SearchService,
    private trackService: TrackService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.searchService.searchTracks(params.searchTerm)
        .then(result => {
          this.tracks = result;
        });
    });
  }

  toggleFavorite (track: any) {
    this.trackService.toggleFavorite(track.id)
      .then(result => {
        track.isFavorite = !track.isFavorite;
      });
  }

  onTrackClicked (track: any) {
    this.tracks.map((t: any) => t.isPlaying = false)
    track.isPlaying = true;
    
    this.appService.onTrackClick.emit(track);
  }
}
