import { Injectable, EventEmitter } from '@angular/core';

import { colorThemes } from '../../config/color-themes';
import { ApiService } from './api.service';

const colorThemeKey = "colorThemeIndex";

@Injectable()
export class AppService {

  onTrackClick = new EventEmitter<any>();
  onTrackEnd = new EventEmitter<any>();
  onSyncComplete = new EventEmitter<void>();

  playingTrack: any;
  currentColorThemeIndex: number = 0;
  isMainMenuOpened: boolean = false;

  constructor (private apiService: ApiService) {
    this.onTrackClick.subscribe((track: any) => {
      this.playingTrack = track;
    });

    const storedColorTheme = localStorage.getItem(colorThemeKey);
    if (storedColorTheme) {
      this.currentColorThemeIndex = parseInt(storedColorTheme);
      this.setColorTheme();
    }
  }

  private setColorTheme () {
    document.body.classList.add(colorThemes[this.currentColorThemeIndex]);
  }

  async updateDatabase (): Promise<any> {
    return await this.apiService.get("sync", false);
  }

  collapseMainMenu (): void {
    const navbarToggler = document.querySelector(".navbar-toggler") as HTMLElement;
    navbarToggler.click();
  }

  // TODO: better removal of current color theme class from <body>
  changeColorTheme (): void {
    this.currentColorThemeIndex++;
    if (this.currentColorThemeIndex >= colorThemes.length) {
      this.currentColorThemeIndex = 0;
    }

    const { classList } = document.body;
    while (classList.length > 0) {
      classList.remove(classList.item(0));
    }

    this.setColorTheme();
    localStorage.setItem(colorThemeKey, this.currentColorThemeIndex.toString());
  }
}
