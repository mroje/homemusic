import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

@Injectable()
export class BandService {

  constructor (private apiService: ApiService) {}

  async getAll (): Promise<any> {
    return await this.apiService.get("bands");
  }

  async getOneById (bandId: number): Promise<any> {
    return await this.apiService.get(`bands/${bandId}`);
  }
}
