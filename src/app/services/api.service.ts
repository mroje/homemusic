import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  constructor (private http: Http) {}

  async get (endpoint: string, jsonResponse: boolean = true): Promise<any> {
    try {
      const response = await this.http.get(`${environment.url}api/${endpoint}`).toPromise();
      return jsonResponse ? response.json() : response;
    }
    catch (error) {
      return Promise.reject(error);
    }
  }

  async post (endpoint: string, body: any): Promise<any> {
    try {
      const response = await this.http.post(`${environment.url}api/${endpoint}`, body).toPromise();
      return response;
    }
    catch (error) {
      return Promise.reject(error);
    }
  }
}
