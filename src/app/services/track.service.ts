import * as _ from 'lodash';

import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

const queuedTracksKey: string = 'queued-tracks';

@Injectable()
export class TrackService {

  constructor (private apiService: ApiService) {}

  private getStoredQueuedTracks() {
    return JSON.parse(localStorage.getItem(queuedTracksKey)) || [];
  }

  private storeQueuedTracks (tracks: any[]) {
    localStorage.setItem(queuedTracksKey, JSON.stringify(tracks));
  }

  async toggleFavorite (trackId: number): Promise<any> {
    return await this.apiService.post(`tracks/${trackId}/toggle-favorite`, {});
  }

  async getFavorites(): Promise<any> {
    return await this.apiService.get("tracks/favorites");
  }

  async getQueued(): Promise<any> {
    let tracks = this.getStoredQueuedTracks();

    return Promise.resolve(tracks);
  }

  async isQueued (track: any): Promise<boolean> {
    let tracks = this.getStoredQueuedTracks();

    let foundTrack = _.find(tracks, (t: any) => { return t.id === track.id; });
    return Promise.resolve(!!foundTrack);
  }

  enqueue (track: any): void {
    let tracks = this.getStoredQueuedTracks();

    tracks.push(track);
    this.storeQueuedTracks(tracks);
  }

  dequeue(track: any): void {
    let tracks = this.getStoredQueuedTracks();
    
    _.remove(tracks, (t: any) => { return t.id === track.id; });
    this.storeQueuedTracks(tracks);
  }
}
