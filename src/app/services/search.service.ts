import { Injectable } from '@angular/core';

import { ApiService } from './api.service';

@Injectable()
export class SearchService {

  constructor (private apiService: ApiService) {}

  async searchTracks (searchTerm: string): Promise<any> {
    return await this.apiService.get(`tracks/search?term=${searchTerm}`);
  }
}
