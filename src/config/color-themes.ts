export const colorThemes = [
  'light-theme',
  'dark-theme',
  'solarized-light-theme',
  'red-theme'
];
