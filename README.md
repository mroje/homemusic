# HomeMusic
***
### This is a web app for accessing music from home, written by me using Angular.

## Usage:
***
#### Precondition is that [*HomeMusic.NodeApi*](https://bitbucket.org/mroje/homemusic.nodeapi/src/master/) server must be set up.

### HomeMusic app setup:
1. Download the project.
2. In **./package.json** under *"scripts"* on *"serve:prod"* line, change *host* to your local IPv4 address and *port* to whichever you like.
3. in **./src/environments/environment.prod.ts** file, change *url* to your local IPv4 address with port 7000 (port needs to be the same as one [*HomeMusic.NodeApi*](https://bitbucket.org/mroje/homemusic.nodeapi/src/master/) server listens on)
4. Using terminal navigate to project directory and run **npm run serve:prod** command to build and run Angular's app server
5. Open the browser and navigate to specified host and port (in **./package.json**)

## Screenshots:
***
![screenshots](https://bitbucket.org/mroje/homemusic/raw/c8bcf47e7cb6715bcbe48a98153188a9a4dda8c8/src/assets/images/screenshots.png)
